from django.contrib import admin
from django.urls import path, include
from core import views


app_name="core"
urlpatterns=[
    path('', views.adm.as_view(), name="adm"),

    path('camping/', views.Campaign.as_view(), name="campaign"),
    path('detail/camping/<int:pk>/', views.DetailCampaign.as_view(), name='detail_campaign'),
    path('update/camping/<int:pk>/', views.UpdateCampaign.as_view(), name='update_campaign'),
    path('delete/camping/<int:pk>/', views.DeleteCampaign.as_view(), name='delete_campaign'),
    path('new/camping/', views.NewCampaign.as_view(), name='Newcampaign'),

    path('list/gender/', views.list_gender.as_view(), name="gender"),
    path('detail/gender/<int:pk>/', views.DetailGender.as_view(), name='detail_gender'),
    path('update/gender/<int:pk>/', views.UpdateGender.as_view(), name='update_gender'),
    path('delete/gender/<int:pk>/', views.DeleteGender.as_view(), name='delete_gender'),
    path('new/gender/', views.NewGender.as_view(), name='Newgender'),

    path('list/category/', views.list_category.as_view(), name="category"),
    path('detail/category/<int:pk>/', views.DetailCategory.as_view(), name='detail_category'),
    path('detail/category/<int:pk>/', views.DetailCategory.as_view(), name='detail_category'),
    path('update/category/<int:pk>/', views.UpdateCategory.as_view(), name='update_category'),
    path('delete/category/<int:pk>/', views.DeleteCategory.as_view(), name='delete_category'),
    path('new/category/', views.NewCategory.as_view(), name='Newcategory'),

    path('list/payment/', views.list_payment.as_view(), name="payment"),
    path('detail/payment/<int:pk>/', views.DetailPayment.as_view(), name='detail_payment'),
    path('update/payment/<int:pk>/', views.UpdatePayment.as_view(), name='update_payment'),
    path('delete/payment/<int:pk>/', views.DeletePayment.as_view(), name='delete_payment'),
    path('new/payment/', views.NewPayment.as_view(), name='Newpayment'),

    path('product/', views.Product.as_view(), name="product"),
    path('recover/product/<int:pk>/', views.RecoverProduct.as_view(), name='recover_product'),
    path('update/product/<int:pk>/', views.UpdateProduct.as_view(), name='update_product'),
    path('delete/product/<int:pk>/', views.DeleteProduct.as_view(), name='delete_product'),
    path('new/product/', views.NewProduct.as_view(), name='new_product'),
   
]



